import os
from datetime import datetime
from pathlib import Path

from tools.OSMnxPointInsideBuildingTool.write_readme import write_readme

from main import do_inside_building

task_file = Path("task.txt")
tolerance_radii = [0, 6]

output_dir = Path("outputs", datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))

if __name__ == "__main__":
    for radius in tolerance_radii:
        if radius == 0:
            suffix = "_basic"
        else:
            suffix = f"_tolerance_{radius}"

        specific_output_dir = output_dir / f"output{suffix}" / ""

        os.makedirs(specific_output_dir)

        do_inside_building(task_file, specific_output_dir, radius)

        with open(output_dir / f"readme{suffix}.txt", "w") as f:
            write_readme(f, radius)
