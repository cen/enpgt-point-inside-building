import logging
import math
import os
import shutil
import sys
from ast import literal_eval
from collections.abc import Mapping
from dataclasses import replace
from datetime import datetime
from multiprocessing import Pool, cpu_count
from pathlib import Path
from types import MappingProxyType
from typing import NamedTuple, Optional

import enpgt_task_file_reader.settings
import osmnx
import osmnx._errors
import osmnx.features
import osmnx.settings
import pyproj
import shapely.ops
from enpgt_task_file_reader import PolygonData, convert_nsv_task_file_to_polygons
from enpgt_task_file_reader.utils import match_polygon_data_results_to_geo_ids, reverse_geo_id_to_polygon_data
from shapely import Point
from tqdm import tqdm

import settings
from write_readme import write_readme


class PointInsideBuildingResult(NamedTuple):
    radius: float
    directly_inside_building: bool | str = ""
    building_within_radius: bool | str = ""
    lowest_distance: float | str = ""
    likelihood: float | str = ""
    building_area: float | str = ""
    osm_type: str | str = ""
    osm_id: str | str = ""
    tags: Mapping = MappingProxyType({})
    error_reason: str | str = ""


def do_single_inside_building(polygon_data: PolygonData) -> PointInsideBuildingResult:
    lng, lat = polygon_data.original_x, polygon_data.original_y
    accuracy = polygon_data.accuracy

    tags = {"building": True}

    while True:
        for search_radius in range(math.ceil(float(accuracy)) + settings.RADIUS_STEP, settings.MAX_RADIUS, settings.RADIUS_STEP):
            try:
                geoms = osmnx.features.features_from_point((lat, lng), tags, search_radius)
            except osmnx._errors.InsufficientResponseError:
                continue
            if not geoms.empty:
                break
        else:
            return PointInsideBuildingResult(accuracy, error_reason=f"Max radius {settings.MAX_RADIUS}m exceeded.")

        crs_aeqd = pyproj.Proj(proj="aeqd", datum="WGS84", lon_0=lng, lat_0=lat, units="m")

        geoms = geoms.to_crs(crs_aeqd.definition_string())

        lowest_dist = math.inf
        closest_feature = None
        closest_geom = None

        for feature in geoms.iterfeatures():
            geom = shapely.ops.shape(feature["geometry"])

            dist = shapely.distance(geom, Point(0, 0))

            if dist < lowest_dist:
                lowest_dist = dist
                closest_feature = feature
                closest_geom = geom

        if lowest_dist >= search_radius:
            # Might not actually be the closest
            continue
        break

    osm_id = literal_eval(closest_feature["id"])
    base_info = {"osm_type": osm_id[0], "osm_id": osm_id[1], "area": closest_geom.area}
    closest_feature = {
        k: v
        for k, v in closest_feature["properties"].items()
        if not k.startswith("name")
        and not k.startswith("addr")
        and not k.startswith("contact")
        and k not in {"building", "nodes", "ways", "type", "wikidata"}
        and k not in base_info
        and v is not None
    }

    multipoly = shapely.unary_union(geoms["geometry"])

    is_inside_building = multipoly.intersects(Point(0, 0))

    if not accuracy:
        likelihood = is_inside_building * 100
    else:
        radius_poly = Point(0, 0).buffer(accuracy, resolution=30)
        area_before = radius_poly.area

        intersection = multipoly.intersection(radius_poly)
        area_after = intersection.area

        likelihood = min(1.0, area_after / area_before)

    return PointInsideBuildingResult(
        accuracy,
        is_inside_building,
        is_inside_building or lowest_dist <= accuracy,
        lowest_dist,
        likelihood,
        base_info["area"],
        base_info["osm_type"],
        base_info["osm_id"],
        closest_feature,
    )


def initializer(osm_url: Optional[str]):
    if osm_url is None:
        logging.info("Default OSM endpoint used.")
        return

    osmnx.settings.overpass_endpoint = osm_url
    osmnx.settings.overpass_rate_limit = False
    osmnx.settings.use_cache = False
    logging.info(f"Using OSM endpoint {osm_url}")


def do_inside_building(infile: Path, output_dir: Path, default_radius: Optional[float], osm_url: Optional[str] = None):
    logging.info("---")
    logging.info(
        f"Getting likelihood of points being inside a building for file {infile}.\n"
        f"If no radius parameter is set per location, {default_radius}m will be used as the default radius."
    )

    shutil.copyfile(infile, Path(output_dir, infile.name))
    outfile = Path(output_dir, "output.csv")

    enpgt_task_file_reader.settings.multiprocessing_cores = settings.MULTIPROCESSING_CORES
    geo_id_to_polygon_data = convert_nsv_task_file_to_polygons(infile, 0)

    for geo_id, polygon_data in geo_id_to_polygon_data.items():
        if polygon_data.accuracy is None:
            if default_radius is None:
                raise RuntimeError("PolygonDatas without accuracy present & no default accuracy radius provided.")
            geo_id_to_polygon_data[geo_id] = replace(polygon_data, accuracy=default_radius)

    polygon_data_to_geo_id = reverse_geo_id_to_polygon_data(geo_id_to_polygon_data)
    polygon_datas = list(polygon_data_to_geo_id)

    output: dict[PolygonData, PointInsideBuildingResult]
    if settings.MULTIPROCESSING_CORES in (0, 1):
        initializer(osm_url)
        output = {polygon_data: do_single_inside_building(polygon_data) for polygon_data in tqdm(polygon_datas)}
    else:
        multiprocessing_cores = settings.MULTIPROCESSING_CORES
        if multiprocessing_cores < 0:
            multiprocessing_cores = cpu_count()

        with Pool(multiprocessing_cores, initializer=initializer, initargs=(osm_url,)) as pool:
            output = dict(
                zip(polygon_datas, tqdm(pool.imap(do_single_inside_building, polygon_datas), total=len(polygon_datas)))
            )

    output_by_geo_id = match_polygon_data_results_to_geo_ids(output, polygon_data_to_geo_id, geo_id_to_polygon_data)

    with open(outfile, "w") as output_file:
        output_file.write(
            "id,"
            "radius,directly_inside_building,building_within_radius,"
            "distance_to_nearest_building,inside_building_probability,"
            "accuracy_circle_area,building_area,"
            "osm_type,error_reason,tags\n"
        )

        for geo_id, single_output in output_by_geo_id.items():
            output_file.write(
                f"{geo_id},"
                f"{single_output.radius},{single_output.directly_inside_building},{single_output.building_within_radius},"
                f"{single_output.lowest_distance},{single_output.likelihood},{single_output.radius ** 2 * math.pi},"
                f"{single_output.building_area},{single_output.osm_type},{single_output.error_reason},"
                f"\"{','.join([str(k) + ':' + str(v) for k, v in single_output.tags.items()])}\"\n"
            )


def main(args):
    default_task_file = Path("task.txt")

    radius = 6

    osm_url = None

    for arg in args:
        if arg.isnumeric():
            radius = int(arg)

        elif arg.startswith("osm_url="):
            osm_url = arg[8:]
            if not osm_url.endswith("api"):
                osm_url = osm_url.rstrip("/") + "/api"

        elif Path(arg).is_file():
            default_task_file = Path(arg)

        else:
            logging.info(f"Could not find file {arg}.")
            return

    if not default_task_file.is_file():
        logging.info(f"No task file was specified, and {default_task_file} was not found. Don't know what to work on.")
        return

    output_dir = Path("outputs", datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))
    os.makedirs(output_dir)

    do_inside_building(default_task_file, output_dir, radius, osm_url)

    with open(Path(output_dir, "readme.txt"), "w") as f:
        write_readme(f, radius)

    return


if __name__ == "__main__":
    main(sys.argv[1:])
