from typing import TextIO


def write_readme(f: TextIO, radius: int):
    f.write(
        "POINT INSIDE BUILDING\n\n"
        "This file contains some information on how the data was generated and how to interpret it."
    )

    f.write(
        "\n\n-----\n\n"
        "Terrain Height DATASET\n\n"
        "This tool uses OSM building data to check whether a coordinate is inside of a building."
    )

    f.write(
        "\n\n-----\n\n"
        "INPUT AND OUTPUT\n\n"
        "Each line in the task file defines a location.\n"
        "Optionally, each line can define a radius to specify the accuracy of the measurement."
        f" Otherwise, the default radius {radius} will be used.\n\n"
        "The output will contain, for each coordinate,"
        " the likelihood of that point sitting inside of a building"
        " as well as metadata about the closest building."
    )

    f.write("\n\n-----\n\nWHAT EXACTLY WAS DONE FOR THIS TASK?\n\n")

    f.write(
        "For each location, a request to OSM is made for all the buildings around it.\n"
        "Then, a circle is drawn around the location coordinate using the accuracy radius.\n"
        f"This accuracy is {radius} by default, but each location can also define its own accuracy.\n"
        "This circle is then overlayed onto the building data.\n"
        "The likelihood of the coordinate being inside a building is calculated by the percentage of the"
        " circle's area that overlaps with a building.\n\n"
    )

    f.write("\n-----\n\nOUTPUT\n\n")

    f.write(
        "Each output has the following parameters:\n\n"
        "id - Unique identifier for location.\n"
        "radius - Accuracy in meters used to calculate the likelihood of this coordinate being in a building.\n"
        "directly_inside_building - True if the location is directly inside of a building.\n"
        "building_within_radius - True if there are any buildings within the tolerance radius.\n"
        "distance_to_nearest_building - Distance to nearest building in meters.\n"
        "inside_building_probability - Confidence that the point is within a building given the specified tolerance"
        " radius. This is given by the area of buildings in the radius / the area of the circle given by the radius.\n"
        "accuracy_circle_area - Area of the circle made by the tolerance radius.\n"
        "building_area - Area of only the nearest building.\n"
        "osm_type- The type of OSM object of the nearest building."
        "osm_id - The unique ID used by the nearest building on OSM's servers.\n"
        "tags - Additional info about the building.\n"
    )
