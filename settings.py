MULTIPROCESSING_CORES = -1
# Enable multiprocessing with as many cores as specified.
# 1 / 0 / None = No multiprocessing
# -1 = Use multiprocessing with system default amount of cores

MAX_RADIUS = 2000
# Maximum radius (in meters) to look for buildings for.

RADIUS_STEP = 30
# Step to increase radius by if no buildings were found
