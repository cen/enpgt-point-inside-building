# ENPGT Point Inside Building

_Part of the "EnvNeuro Python GIS Toolbox", a Python project for extracting multiple types of geodata from large datasets, for lists of coordinates from participants in EnvNeuro studies. This overarching project is currently in early theoretical stages, although individual tools like this one are already in a usable standalone state._

This is a tool that, for a given input of coordinates, checks whether each of them is inside a building or not using Openstreetmap data.

Usage:
1. Install Python requirements
2. Make sure you have a task file inside the root directory. The support here is currently specifically tailored towards one dataset, others may or may not work. Your dataset must be in WGS84. Make sure each line has id, lat and lng.
3. Run `python main.py [filename]`.
