import os.path
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

from BaseClasses import ToolDefinition, ToolDefinitionArgs
from pyproj import Transformer
from tools.OSMnxPointInsideBuildingTool.main import main


class OSMnxPointInsideBuildingToolClass(ToolDefinition):
    tool_name: str = "OSMnxPointInsideBuildingTool"
    metric_name: str = "Is Point Inside Building"
    supports_multipoly_mode = False

    transproj = Transformer.from_crs(
        "WGS84",
        "EPSG:3035",
        always_xy=True,
    )

    @dataclass
    class OSMnxPointInsideBuildingToolArgs(ToolDefinitionArgs):
        radius: Optional[int] = None
        osmnx_endpoint: Optional[str] = None

    @classmethod
    def supports_coordinate(cls, lat: float, lng: float) -> bool:
        return True

    @classmethod
    def required_setup(cls, _: OSMnxPointInsideBuildingToolArgs):
        return

    @classmethod
    def main(cls, args: OSMnxPointInsideBuildingToolArgs) -> Path:
        sysargs = [os.path.realpath(args.task_file_location)]
        if args.radius is not None:
            sysargs.append(str(args.radius))
        if args.osmnx_endpoint is not None:
            sysargs.append("osm_url=" + args.osmnx_endpoint)

        correct_dir = os.getcwd()

        tool_dir = os.path.dirname(os.path.realpath(__file__))

        output_directories_before = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_before = [
                Path(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        os.chdir(tool_dir)

        main(sysargs)

        os.chdir(correct_dir)

        output_directories_after = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_after = [
                Path(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        new_output_dirs = list(set(output_directories_after) - set(output_directories_before))
        assert len(new_output_dirs) == 1

        # TODO: Write readme

        return new_output_dirs[0]
